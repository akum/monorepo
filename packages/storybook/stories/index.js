import React, {Fragment} from 'react';
import { storiesOf } from '@storybook/react';

import { Button, Modal, Input, ToggleHOC } from '@workspace/base'


storiesOf('Button', module)
        .add('Btn', () => (
            <Button>Hello Button</Button>
        )).add('btn wi', () => (
            <Button>
                <span role="img" aria-label="so cool">😀 </span>
            </Button>
        ));

storiesOf('MODAL', module)
        .add('modal', () => (<ToggleHOC>
            {({on, toggle}) => <Fragment>
                <Button onClick={toggle}>Показать модальное окно</Button>
                <Modal on={on} onToggle={toggle}>
                    demo
                </Modal>
            </Fragment>
        }
        </ToggleHOC>))
        .add('modal2', () => (<ToggleHOC>
            {({on, toggle}) => <Fragment>
                        <Button onClick={toggle}>Показать модальное окно</Button>
                        <Modal on={on} onToggle={toggle} />
                </Fragment>}
        </ToggleHOC>))

storiesOf('INPUT', module)
.add('1', () => (
    <Input onChange={(value) => {
            console.log(value)
    }} value={'1'}/>
)).add('2', () => (
    <Input />
));

