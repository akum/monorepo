const {resolve} = require('path');

module.exports = {
    entry: resolve(__dirname, './Components'),
    output: {
        path: resolve(__dirname),
        filename: 'index.js',
        library: '',
        libraryTarget: 'commonjs'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader?cacheDirectory',
                options: {
                    presets: ['@babel/preset-env', '@babel/react'],
                    plugins: [
                        "babel-plugin-styled-components",
                        "@babel/plugin-proposal-class-properties"
                    ]
                }
            }
        ]
    }
};