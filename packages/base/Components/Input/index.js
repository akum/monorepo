import React, { PureComponent } from 'react';

export default class Input extends PureComponent {
    state = {
        value: '',
    };
    
    onChange = (value) => {
        if (this.props.onChange) {
            return this.props.onChange(value);
        }
        this.setState({value});
    }
    
    render() {
        return (
            <div>
                <input
                    value={this.props.value}
                    onChange={this.props.change}
                />
            </div>
        );
    }
}