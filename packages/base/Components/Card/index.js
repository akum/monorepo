import styled from 'styled-components';
import {styles} from '../utils';

const Card = styled.div`
	background:white;
	border-radius: 5px;
	padding: 15px;
	color: ${styles.colorblack};
	${styles.elevat[4]};
	${styles.transition({prop: 'box-shadow', ease: 'ease-in'})};
	&:hover {
		${styles.elevat[5]};
	}
`;

export default Card;