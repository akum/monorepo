import React, { PureComponent } from 'react';
import styled from 'styled-components'
import { Transition, animated } from 'react-spring/renderprops'
import { Portal, ToggleClass, styles } from '../utils';
import Card from '../Card';
class Modal extends PureComponent {
    onToggle = () => {
        this.props.onToggle()
        // this.props.toggle()
    }
    
    render() {
        const {children, on} = this.props;
        return (<Portal>
                <Transition
                    items={on}
                    native
                    config={{
                        duration:300,
                    }}
                    from={{
                        opacity: 0,
                        transform : 'translateY(-100%)'
                    }}
                    enter={{
                        opacity: 1,
                        transform : 'translateY(0)'
                    }}
                    leave={{
                        opacity: 0,
                        transform : 'translateY(-100%)'
                    }}
                >
                    {(on) => on && ((style) => (
                        <ModalWrapper>
                            <Background
                                onClick={this.onToggle}
                                style={{opacity: style.opacity}}
                            />
                                <ModalCard
                                    // forwardedAs={Card}
                                    as={Card}
                                    style={style}>
                                    {children || <h1>MODAL NOT CONTENT</h1>}
                                </ModalCard>
                        </ModalWrapper>
                    ))}
                </Transition>
            </Portal>
        );
    }
}
const ModalWrapper = styled.div`
	${styles.absolute({})};
	width: 100%;
	height: 100%;
	background: rgba(16, 127, 127, .1);
	
	display: flex;
	justify-content:center;
	align-items: center;
`;

const Background = styled(animated.div)`
	${styles.absolute({})};
	width: 100%;
	height: 100%;
	background: ${styles.colors.black};
	opacity:.5;
`;

const ModalCard = styled(animated.div)`
	position:relative;
	min-width: 320px;
	z-index: 10;
	margin-bottom: 100px;
`;

export default Modal;