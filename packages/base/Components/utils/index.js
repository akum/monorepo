import Portal from './Portal'
import ToggleClass from './ToggleClass'
import ToggleHOC from './ToogleHOC'
import * as colors from './styles/colors'
import absolute from './styles/Absolute'
import elevat from './styles/elevat'
import transition from './styles/transition'

const styles = {
    absolute,
    colors,
    elevat,
    transition
};
export {
    Portal,
    ToggleClass,
    ToggleHOC,
    styles
}