import { createElement, Component } from 'react';
import hoist from 'hoist-non-react-statics';

export default  ({
    onToggle = () => {}
} = {}) => (OriginalComponent) => {
    class WrappedComponent extends Component {
        state = {
            on: false
        }
        
        onToggle = (...arg) => {
            this.setState((state => ({
                on: !state.on
            })))
        }
        
        render() {
            const props = {
                ...this.props,
                on: this.state.active,
                onToggle: this.onToggle
            };
            
            return createElement(OriginalComponent, props);
        }
    }
    return hoist(WrappedComponent, Component);
};