import {Component} from 'react'
import ReactDOM from 'react-dom'

const portalRoot = document.getElementById('root');

export default class extends Component {
    constructor () {
        super()
        this.el = document.createElement('div')
    }
    
    componentDidMount () {
        console.log(portalRoot)
        portalRoot.appendChild(this.el)
    }
    
    componentWillUnmount() {
        portalRoot.removeChild(this.el)
    }
    
    render () {
        const {children} = this.props;
        console.log(portalRoot)
        return ReactDOM.createPortal(children, this.el)
    }
};