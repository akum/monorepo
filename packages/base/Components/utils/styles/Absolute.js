 export default ({
        x = 'left',
        y = 'top'
    }) =>`
	position: absolute;
	${x}:0;
	${y}:0;
`;