export default ({prop = 'all', length = '.3s', ease = 'ease'}) => `
	transition: ${prop} ${length} ${ease};
`;