import React, { Fragment } from 'react';
import {Button, Modal, ToggleHOC} from '@workspace/base';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Hello monorepo and worspaces1
        </p>
        
        <Button>1111</Button>
      </header>
        <ToggleHOC>
            {({on, toggle}) => <Fragment>
                <Button onClick={toggle}>Показать модальное окно</Button>
                <Modal on={on} onToggle={toggle}>
                    demo
                </Modal>
            </Fragment>
            }
        </ToggleHOC>
    </div>
  );
}

export default App;
